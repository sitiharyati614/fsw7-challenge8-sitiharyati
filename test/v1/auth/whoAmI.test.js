const request = require('supertest');
const bcrypt = require('bcryptjs');
const app = require('../../../app');
const { User } = require('../../../app/models');
const { AuthenticationController } = require('../../../app/controllers');

const password  = 'admin12';
const userAdmin = {
    name: 'Admin',
    email: 'Admin@binar.co.id',
    encryptedPassword: bcrypt.hashSync(password, 10),
    roleId: 2,
};

const userCustomer = {
    name: 'Customer',
    email: 'customer@binar.co.id',
    encryptedPassword: bcrypt.hashSync(password, 10),
    roleId: 1,
};

describe('GET /v1/auth/whoami', () => {
    beforeAll(async () => {
      try {
        await User.create(userAdmin);
        await User.create(userCustomer);
      } catch (err) {
        console.error(err.message);
      }
    });
  
    afterAll(async () => {
      try {
        await User.destroy({
          where: {
            email: userAdmin.email,
          },
        });
        await User.destroy({
          where: {
            email: userCustomer.email,
          },
        });
      } catch (err) {
        console.error(err.message);
      }
    });
  
    describe('GET should response with 401 as status code', () => {
      let tokenWhoAmIAdmin;
      beforeEach(async () => {
        await request(app)
          .post('/v1/auth/login')
          .set('Content-Type', 'application/json')
          .send({ email: userAdmin.email, password })
          .then((AdminWhoAmILogin) => {
            tokenWhoAmIAdmin = AdminWhoAmILogin.body.accessToken;
          });
      });
      it('Admin cannot be get who Am I', async () => await request(app)
        .get('/v1/auth/whoami')
        .set('authorization', `Bearer ${tokenWhoAmIAdmin}`)
        .then((adminResWhoAmI) => {
          expect(adminResWhoAmI.statusCode).toBe(401);
          expect(adminResWhoAmI.body).toEqual({
            error: {
              name: 'Error',
              message: 'Access forbidden!',
              details: {
                role: 'ADMIN',
                reason: 'ADMIN is not allowed to perform this operation.',
              },
            },
          });
        }));
    });
    describe('GET should respond with 200 as status code', () => {
      let tokenWhoAmICustomer;
      beforeEach(async () => {
        await request(app)
          .post('/v1/auth/login')
          .set('Content-Type', 'application/json')
          .send({ email: userCustomer.email, password })
          .then((CustomerWhoAmILogin) => {
            tokenWhoAmICustomer = CustomerWhoAmILogin.body.accessToken;
          });
      });
      it('Customer access to GET Who Am I', async () => await request(app)
        .get('/v1/auth/whoami')
        .set('authorization', `Bearer ${tokenWhoAmICustomer}`)
        .then((customerResWhoAmI) => {
          expect(customerResWhoAmI.statusCode).toBe(200);
          expect(customerResWhoAmI.body.name).toEqual(userCustomer.name);
          expect(customerResWhoAmI.body.email).toEqual(
            userCustomer.email.toLowerCase(),
          );
        }));
    });
  });
  

  describe('#handleGetUser', () => {
    const userNotFound = {
      user: {
        id: 1000,
        name: 'User Not Found Test',
        email: 'usernotFound@mail.com',
        role: { id: 1, name: 'CUSTOMER' },
      },
    };
    beforeEach(async () => {
      try {
        const checkUserNotFound = await User.findByPk(userNotFound.id);
        if (checkUserNotFound) {
          await User.destroy({
            where: {
              id: userNotFound.id,
            },
          });
        }
      } catch (err) {
        console.error(err);
      }
    });
    describe('GET should response with 404 as status code', () => {
      const UserRoleNotFound = {
        id: 3000,
        roleId: 10,
      };
      it('User not found', async () => {
        const mockAuthModel = {};
        mockAuthModel.findByPk = jest.fn();
        const mockResponse = {};
        mockResponse.status = jest.fn().mockReturnThis();
        mockResponse.json = jest.fn().mockReturnThis();
        const authController = new AuthenticationController({
          userModel: mockAuthModel,
        });
        await authController.handleGetUser(userNotFound, mockResponse);
        expect(mockResponse.status).toHaveBeenCalledWith(404);
        expect(mockResponse.json).toHaveBeenCalledWith(expect.anything());
      });
      it('Role not found', async () => {
        const mockTask = new User(UserRoleNotFound);
        const mockAuthModel = {};
        mockAuthModel.findByPk = jest.fn().mockReturnValue(mockTask);
        const mockAuthModelRole = {};
        mockAuthModelRole.findByPk = jest.fn();
        const mockResponse = {};
        mockResponse.status = jest.fn().mockReturnThis();
        mockResponse.json = jest.fn().mockReturnThis();
        const authController = new AuthenticationController({
          roleModel: mockAuthModelRole,
          userModel: mockAuthModel,
        });
        await authController.handleGetUser(
          { user: { id: UserRoleNotFound.id } },
          mockResponse,
        );
        expect(mockResponse.status).toHaveBeenCalledWith(404);
        expect(mockResponse.json).toHaveBeenCalledWith(expect.anything());
      });
    });
  });